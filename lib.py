import datetime
import sys

from csv import DictReader
from typing import Any, Dict, Generic, Iterable, List, Mapping, NewType, Optional, Sequence, Set, Tuple, TypeVar, Union
from typing import overload
from typing_extensions import Literal, Protocol, TypedDict

import pandas as pd
import PyInquirer # type: ignore

# config
MIN_DIVERGENCE_FRACTION = 10 / 100. # percent


# allocation fraction, minimum value
Allocation = Tuple[float, int]


class ValueEvent:
    date: str
    key: str
    value: int
    def __init__(self, date: str, key: str, value: int) -> None:
        self.date = date
        self.key = key
        self.value = value

class TransferEvent:
    date: str
    from_key: str
    to_key: str
    from_amt: int
    to_amt: int

    def __init__(self, date: str, from_key: str, to_key: str, from_amt: int, to_amt: int) -> None:
        self.date = date
        self.from_key = from_key
        self.to_key = to_key
        self.from_amt = from_amt
        self.to_amt = to_amt


Transaction = Union[ValueEvent, TransferEvent]

class TransactionLog:
    txns: List[Transaction]
    locked: List[str]

    def __init__(self, txns: List[Transaction], locked: List[str]) -> None:
        self.txns = txns
        self.txns.sort(key=lambda t: t.date)
        self.locked = list(set(locked))

    def state(self, before: Optional[str] = None) -> pd.DataFrame:
        result: Dict[str, int] = {}

        for txn in self.txns:
            if before and (not txn.date or txn.date >= before):
                continue

            if isinstance(txn, ValueEvent):
                result[txn.key] = txn.value
            elif isinstance(txn, TransferEvent):
                if txn.from_key not in result:
                    result[txn.from_key] = 0
                if txn.to_key not in result:
                    result[txn.to_key] = 0

                result[txn.from_key] += txn.from_amt
                result[txn.to_key] += txn.to_amt

        rows = [{'key': k, 'value': v} for k, v in result.items()
                if v != 0]
        return pd.DataFrame(rows)


    def adjust_csv(self, file: str, key: str, value: str, date: Optional[str] = None) -> 'TransactionLog':
        tmp = self
        with open(file, 'r') as fp:
            reader = DictReader(fp)
            for row in reader:
                row_key = row[key]
                row_value = int(float(row[value]) * 100)
                tmp = tmp.adjust(key=row_key, value=row_value, date=date)
        return tmp


    def adjust(self, key: str, value: int, date: Optional[str] = None) -> 'TransactionLog':
        if not date:
            date = '9999-99-99'
        return self.add(ValueEvent(date, key, value))

    def buy(self, key: str, cash: int, value: int, date: Optional[str] = None) -> 'TransactionLog':
        if not date:
            date = '9999-99-99'
        return self.add(TransferEvent(
            date,
            from_key='cash',
            from_amt=-cash,
            to_key=key,
            to_amt=value,
        ))

    def sell(self, key: str, cash: int, value: int, date: Optional[str] = None) -> 'TransactionLog':
        if not date:
            date = '9999-99-99'
        return self.add(TransferEvent(
            date,
            from_key=key,
            from_amt=-value,
            to_key='cash',
            to_amt=cash,
        ))

    def lock(self, key: str) -> 'TransactionLog':
        return TransactionLog(txns=self.txns, locked=[*self.locked, key])

    def add(self, txn: Transaction) -> 'TransactionLog':
        return TransactionLog(txns=[*self.txns, txn], locked=self.locked)


class Allocator:
    _keys: pd.DataFrame

    def __init__(self, classes: Dict[str, Allocation], assignment: Dict[str, List[str]]) -> None:
        total_share = sum([classes[c][0] for c in classes
                           if assignment[c]])
        keys = []
        for klass in classes:
            share, min = classes[klass]
            fraction = share / total_share

            class_keys = assignment[klass]
            for key in class_keys:
                keys.append({
                    'key': key,
                    'fraction': fraction / len(class_keys),
                    'min': min / len(class_keys),
                })

        self._keys = pd.DataFrame(keys)

    def allocation(self) -> pd.DataFrame:
        return self._keys.copy()


def distribute_int_mins(
        frame: pd.DataFrame,
        fraction_key: str, value_key: str, min_key: str,
        locked_keys: List[str]) -> pd.DataFrame:

    any_under = True
    locked_keys = locked_keys[:]
    while any_under:
        frame = distribute_int(frame, fraction_key, value_key, locked_keys)
        any_under = False

        # find a key that's under its minimum
        error = 0
        for i, row in frame.iterrows():
            if row[value_key] < row[min_key]:
                error = row[value_key] - row[min_key]
                frame.loc[i, value_key] -= error # type: ignore
                locked_keys.append(row['key'])
                any_under = True
                break

        # make up the error somewhere else; it'll get spread around
        for i, row in frame.iterrows():
            if row['key'] not in locked_keys:
                frame.loc[i, value_key] += error # type: ignore
                break

    return frame


def distribute_int(
        frame: pd.DataFrame,
        fraction_key: str, value_key: str,
        locked_keys: List[str]) -> pd.DataFrame:

    total_before = frame[value_key].sum()
    frame = distribute_float(frame, fraction_key, value_key, locked_keys)
    frame[value_key] = frame[value_key].astype(int)
    total_after = frame[value_key].sum()
    error = total_before - total_after

    # add the error to the first not-locked key
    for i, row in frame.iterrows():
        if row['key'] not in locked_keys:
            frame.loc[i, value_key] += error # type: ignore
            break

    return frame


def distribute_float(
        frame: pd.DataFrame,
        fraction_key: str, value_key: str,
        locked_keys: List[str]) -> pd.DataFrame:

    # compute scalar totals
    total = frame[~frame['key'].isin(locked_keys)][value_key].sum()
    fraction_total = frame[~frame['key'].isin(locked_keys)][fraction_key].sum()

    # create a delta column
    ideal = frame[fraction_key] * total / fraction_total
    delta = frame[value_key] - ideal
    frame = frame.assign(delta=delta)

    # set locked deltas to zero
    frame.loc[frame['key'].isin(locked_keys), 'delta'] = 0

    # apply the delta column
    frame = frame.assign(**{value_key: frame[value_key] - frame['delta']})
    frame = frame.drop(['delta'], axis=1)

    return frame


class BalanceInfo(TypedDict):
    key: str
    value: int
    ideal: int
    error: int


class Portfolio:
    _allocator: Allocator
    _log: TransactionLog

    def __init__(self, allocator: Allocator, log: TransactionLog) -> None:
        self._allocator = allocator
        self._log = log

    def state(self) -> pd.DataFrame:
        allocation = self._allocator.allocation()
        frame = self._log.state()

        frame = allocation.merge(frame, on='key', how='outer')

        # fill holes from the join
        frame['fraction'] = frame['fraction'].fillna(0)
        frame['min'] = frame['min'].fillna(0)
        frame['value'] = frame['value'].fillna(0)

        # calculate actual fraction
        frame = frame.assign(actual_fraction=frame['value'])
        frame['actual_fraction'] /= frame['value'].sum()

        # calculate the ideal values
        frame = frame.assign(ideal=frame['value'])
        frame = distribute_int_mins(
            frame,
            fraction_key='fraction', value_key='ideal', min_key='min',
            locked_keys=self._log.locked,
        )

        return frame

    def get(self, key: str) -> int:
        frame = self.state()
        return frame.loc[frame['key'] == key]['value'].iloc[0] # type: ignore

    def get_top_sell(self, skip: List[str]) -> Optional[BalanceInfo]:
        frame = self.state()
        frame = frame.assign(error=frame['value'] - frame['ideal'])
        frame = frame.assign(error_fraction=frame['error'] / frame['ideal'])

        # filter to sells
        frame = frame[frame['error'] > 0]
        frame = frame[frame['error_fraction'] > MIN_DIVERGENCE_FRACTION]

        # filter out cash and skipped keys
        frame = frame[frame['key'] != 'cash']
        frame = frame[~frame['key'].isin(skip)]

        if len(frame) == 0:
            return None

        # TODO: do overhead calculations here
        # TODO: make overhead calculation dependent on key

        # put the largest sell on top
        frame = frame.sort_values(by='error_fraction', ascending=False)

        row = frame.iloc[0]
        return {
            'key': row['key'],
            'value': row['value'],
            'ideal': row['ideal'],
            'error': row['error'],
        }

    def get_top_buy(self, skip: List[str]) -> Optional[BalanceInfo]:
        frame = self.state()

        frame = frame.assign(ideal=frame['value'])
        frame = distribute_int_mins(
            frame,
            fraction_key='fraction', value_key='ideal', min_key='min',
            locked_keys=[],
        )

        locked_keys = skip[:]
        # lock remaining sells
        for i, row in frame.iterrows():
            if row['key'] == 'cash':
                continue
            if row['value'] > row['ideal']:
                locked_keys.append(row['key'])


        # remove errors with too much overhead
        any_removed = True
        while any_removed:
            any_removed = False

            frame = frame.assign(ideal=frame['value'])
            frame = distribute_int_mins(
                frame,
                fraction_key='fraction', value_key='ideal', min_key='min',
                locked_keys=locked_keys,
            )
            frame = frame.assign(error=frame['ideal'] - frame['value'])
            frame = frame.assign(error_fraction=frame['error'] / frame['ideal'])

            # find the smallest that has too much overhead
            frame = frame.sort_values(by='error_fraction', ascending=True)
            for i, row in frame.iterrows():
                if row['key'] == 'cash':
                    continue

                error_fraction = row['error_fraction']
                if error_fraction == 0:
                    continue

                if error_fraction < MIN_DIVERGENCE_FRACTION:
                    locked_keys.append(row['key'])
                    any_removed = True
                    break

        # filter to buys
        frame = frame[frame['key'] != 'cash']
        frame = frame[frame['error'] > 0]
        if len(frame) == 0:
            return None

        # put the most diverged buy on top
        frame = frame.sort_values(by='error_fraction', ascending=False)

        row = frame.iloc[0]
        return {
            'key': row['key'],
            'value': row['value'],
            'ideal': row['ideal'],
            'error': row['error'],
        }

    def sell(self, key: str, cash: int, value: int) -> None:
        self._log = self._log.sell(key=key, cash=cash, value=value)

    def buy(self, key: str, cash: int, value: int) -> None:
        self._log = self._log.buy(key=key, cash=cash, value=value)


def brokerage(net_transaction: int) -> int:
    if net_transaction <= 100000:
        return 1000
    elif net_transaction <= 1000000:
        return 1995
    elif net_transaction <= 2500000:
        return 2995
    else:
        return int(int(net_transaction) * 0.0012 * 100)


def input_money_or_cancel(prompt: str) -> Optional[int]:
    def convert(s: str) -> int:
        return int(float(s) * 100)

    def float_validator(s: str) -> Union[bool, str]:
        if s == '':
            return True
        try:
            convert(s)
            return True
        except ValueError:
            return 'Must be a number'

    response = PyInquirer.prompt({
        'type': 'input',
        'name': 'value',
        'message': prompt,
        'validate': float_validator,
    })
    result = response.get('value', None)

    if result is None:
        raise EOFError()
    elif result == '':
        return None
    else:
        return convert(result)
