#!/usr/bin/env python3

import datetime
import math
import sys
from typing import Any, Dict, List, Optional, Sequence, Tuple, Union
from typing_extensions import Literal, TypedDict

import pandas as pd
import tabulate

from lib import Allocator, Allocation, Portfolio
from lib import input_money_or_cancel
import log


def main() -> None:
    allocator = Allocator(log.classes, log.assignment)
    portfolio = Portfolio(allocator, log.log)

    print('Summary')
    print_summary(portfolio)

    print()
    print('SELL')
    do_sell(portfolio)

    print()
    print('BUY')
    do_buy(portfolio)

    print()
    print('Summary')

    # TODO: print new txns
    print_summary(portfolio)


def do_sell(portfolio: Portfolio) -> None:
    locked_keys: List[str] = []
    while True:
        sell = portfolio.get_top_sell(locked_keys)
        if not sell:
            break

        print()
        print('%s is over allocation by $%.2f ($%.2f > $%.2f).'
                % (sell['key'], sell['error'] / 100,
                   sell['value'] / 100, sell['ideal'] / 100))

        subtotal = input_money_or_cancel('Subtotal exc brokerage?')
        total = input_money_or_cancel('Total inc brokerage?')
        if subtotal and total:
            portfolio.sell(key=sell['key'], cash=total, value=subtotal)
        else:
            print('skipping...')

        locked_keys.append(sell['key'])


def do_buy(portfolio: Portfolio) -> None:
    locked_keys: List[str] = []
    while True:
        buy = portfolio.get_top_buy(locked_keys)
        if not buy:
            break

        print('Available cash: %.2f' % (portfolio.get('cash') / 100.))

        print()
        print('%s should be bought for $%.2f.'
                % (buy['key'], buy['error'] / 100.))

        subtotal = input_money_or_cancel('Subtotal exc brokerage?')
        total = input_money_or_cancel('Total inc brokerage?')
        if subtotal and total:
            portfolio.buy(key=buy['key'], cash=total, value=subtotal)
        else:
            print('skipping...')

        locked_keys.append(buy['key'])


def print_summary(portfolio: Portfolio) -> None:
    frame = portfolio.state()

    total = frame['value'].sum()

    items = []
    for i, row in frame.iterrows():
        fraction = row['fraction']
        clamped_fraction = row['ideal'] / total
        value = row['value']
        ideal = row['ideal']
        divergence_fraction = (value - ideal) / ideal
        items.append([
            row['key'],
            '{:.1f}%'.format(fraction * 100) if fraction > 0 else '-',
            '{:.1f}%'.format(clamped_fraction * 100) if clamped_fraction > 0 else '-',
            '{:.2f}'.format(ideal / 100) if ideal != 0 else '-',
            '{:.2f}'.format(value / 100) if value != 0 else '-',
            '{:.1f}%'.format(divergence_fraction * 100) if not math.isnan(divergence_fraction) else '-',
        ])
    items.sort(key=lambda i: i[0])

    items.append([
        'TOTAL',
        '',
        '',
        '',
        '{:.2f}'.format(total / 100),
        '',
    ])

    print(tabulate.tabulate(
        items,
        headers=['Key', 'Allocation', 'Clamped', 'Ideal $', 'Value $', 'Divergence'],
        colalign=['left', 'right', 'right', 'right', 'right', 'right'],
        disable_numparse=True,
    ))


if __name__ == '__main__':
    try:
        main()
    except EOFError:
        pass
